// console.log("Hello World!");

// Arithmetic Operators

let x = 1397, y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

// modulus (%)
let remainder = y % x;
console.log("Result of Modulo operator: " + remainder);

// Assignment Operator
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right opperand to a variable.

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2; //assignmentNumber = 10
console.log("Result of addition assignment operator: " + assignmentNumber);

// This is the shorthand method

assignmentNumber += 2;  //assignmentNumber = 12
console.log("Result of addition assignment operator: " + assignmentNumber);

// (-=, *=, /=) other assignment operator
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result from mdas operation: " + mdas);
/*
    - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
    - The operations were done in the following order:
        1. 3 * 4 = 12
        2. 12 / 5 = 2.4
        3. 1 + 2 = 3
        4. 3 - 2.4 = 0.6
*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

/*
    - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
    - The operations were done in the following order:
        1. 4 / 5 = 0.8
        2. 2 - 3 = -1
        3. -1 * 0.8 = -0.8
        4. 1 + -.08 = .2
*/

// Increment and Decrement
// Operators that add or subtract values by 1 and re-assign the value of the variable.
// Incrementation (++)
// Decrementation (--)

let z = 1;

// The value of z is added by a value of one before returning the value and storing it in the variable increment.

let increment = ++z;
console.log("Result of pre-incrementation: " + increment);
console.log("result of pre-incrementation: " + z);

increment = z++;
console.log("Result of post-incrementation: " + increment);
console.log("result of post-incrementation: " + z);

let decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("result of pre-decrementation: " + z);

decrement = z--;
console.log("Result of post-decrementation: " + decrement);
console.log("result of post-decrementation: " + z);

// Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

/* 
    - Adding/concatenating a string and a number will result is a string
    - This can be proven in the console by looking at the color of the text displayed
    - Black text means that the output returned is a string data type
*/

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// In programming languages "true" = 1, "false" = 0.

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operator

let juan = "juan";

// Equality Operator (==)
// Returns boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log(0 == false);
console.log("juan" == 'juan');
console.log("juan" == juan);

// Inequality Operator (!=) ! = not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log(0 != false);
console.log("juan" != 'juan');
console.log("juan" != juan);

// Strict Equality Operator (===)
// Check if operands are equal/have the same content
// Checks operands if are equal/same data type

console.log(1 === "1");
console.log(0 === false);

// Strict Inequality Operator (!==)
console.log(1 !== "1");
console.log(0 !== false);

//Relational Operators
// Some Comparison operators check whether one value is greater than o less than to the other value.

let a = 50, b = 65;

// GT or Greater Than (>)

let greaterThan = a > b;
console.log(greaterThan);

let lessThan = a < b;
console.log(lessThan);

let GToEqual = a >= b;
console.log(GToEqual);

let LTorEqual = a <= b;
console.log(LTorEqual);

let numStr = "30";
console.log(a > numStr); //true -> forced coercion to change string into number
console.log(b <= numStr);

let str = "twenty";
console.log(b >= str);
//false
// Since the string is not numeric, The string was converted to a number and it resulted to NaN= Not a Number.

// Logical Operators

let isLegalAge = true;
let isRegistered = false;

// Logical AND "&&"  - Double Ampersand
// Returns true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);


// Logical OR "||" -Double Pipe
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical NOT "!" - Exclamation Point
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result from logical NOT Operator: " + someRequirementsNotMet);

